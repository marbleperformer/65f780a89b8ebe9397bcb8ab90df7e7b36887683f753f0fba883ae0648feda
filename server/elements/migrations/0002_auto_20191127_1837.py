# Generated by Django 2.2.7 on 2019-11-27 18:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('elements', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='element',
            name='plot',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='element',
            name='step',
            field=models.PositiveIntegerField(default=1),
        ),
        migrations.AlterField(
            model_name='element',
            name='updated',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
