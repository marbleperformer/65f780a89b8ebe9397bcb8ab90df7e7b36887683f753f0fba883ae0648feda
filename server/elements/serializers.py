from rest_framework.serializers import ModelSerializer, SerializerMethodField

from .models import Element


class ElementSerializer(ModelSerializer):
    function = SerializerMethodField()

    class Meta:
        model = Element
        fields = ('url', 'id', 'function', 'plot',
                  'interval', 'step', 'updated')

    def get_function(self, obj):
        return obj.function.expression
