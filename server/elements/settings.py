from django.conf import settings


GENERATOR_URL = getattr(settings, 'GENERATOR_URL',
                        'http://generator:8001/api/points/series/')


HIGHCHARTS_URL = getattr(settings, 'HIGHCHARTS_URL',
                         'http://highcharts:8080')
