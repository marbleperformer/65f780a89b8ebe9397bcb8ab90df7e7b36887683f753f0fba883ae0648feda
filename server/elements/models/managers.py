from django.db.models import Manager
from django.db.models.signals import post_save

from .signals import set_plot_value


class ElementManager(Manager):
    def contribute_to_class(self, model, name):
        super(ElementManager, self).contribute_to_class(model, name)
        post_save.connect(set_plot_value, model)
