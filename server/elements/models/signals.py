from ..tasks import set_element_plot


def set_plot_value(sender, instance, *args, **kwargs):
    set_element_plot.delay(instance.pk, instance.formula.expression,
                           instance.interval, instance.step)
