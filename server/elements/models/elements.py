from django.db import models

from .managers import ElementManager


class Element(models.Model):
    formula = models.ForeignKey('formulas.Formula', on_delete=models.CASCADE)
    plot = models.CharField(max_length=256, null=True, blank=True)
    interval = models.PositiveIntegerField()
    step = models.PositiveIntegerField(default=1)
    updated = models.DateTimeField(auto_now=True)

    objects = ElementManager()

    def __str__(self):
        return self.formula.expression
