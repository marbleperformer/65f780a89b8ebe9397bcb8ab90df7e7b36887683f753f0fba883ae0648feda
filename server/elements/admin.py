from django.contrib import admin
from django.template.loader import render_to_string

from .models import Element


@admin.register(Element)
class ElementAdmin(admin.ModelAdmin):
    list_display = ('formula', 'plot_image', 'interval',
                    'step', 'updated')
    exclude = ('plot',)

    def plot_image(self, obj):
        return render_to_string(
            'elements/plot-image.html',
            {'image': obj.plot}
        )
