import os
import json
import requests
from datetime import datetime, timedelta
from urllib.parse import urljoin
from hashlib import sha256

from django.apps import apps
from django.core.files.base import ContentFile
from django.core.files.storage import FileSystemStorage
from django.conf import settings

from server.celery import app
from .settings import GENERATOR_URL, HIGHCHARTS_URL


def _get_points(formula, interval, step):
    '''
    private function for getting points collection from generator service
    :param formula: ordinate calculation expression
    :param interval: collection interval
    :param step: collection step
    :return: points collection
    '''
    data = {
        'formula': formula,
        'interval': interval * 24,
        'step': step
    }

    response = requests.post(GENERATOR_URL, data=data)
    if response.status_code == 200:
        data = response.json()
        return data.get('points')


def _get_plot_bytes(formula, categories, series):
    '''
    private function for gettings plot image bytes from highcharts service
    :param formula: ordinate calculation expression
    :param categories: abscissa categories data collection
    :param series: abscissa series data collection
    :return: plot bytes
    '''
    headers = {
        'Content-Type': 'application/json'
    }

    data = {
        'infile': {
            "title": {
                "text": formula
            },
            'xAxis': {
                'categories': categories
            },
            'series': [
                {'data': series}
            ]
        }
    }

    response = requests.post(HIGHCHARTS_URL, headers=headers, data=json.dumps(data))
    if response.status_code == 200:
        return response.content


def _upload_plot_image(pk, plot_bytes):
    '''
    private function for upload plot image file
    :param pk: instance primary key
    :param plot_bytes: plot bytes
    :return: plot image url
    '''

    # creating file name from instance primary key
    hash_obj = sha256()
    pk_bytes = f'{pk}'.encode()
    hash_obj.update(pk_bytes)
    file_name = f'{hash_obj.hexdigest()}.png'

    storage = FileSystemStorage(
        location=os.path.join(settings.MEDIA_ROOT, 'elements'),
        base_url=urljoin(settings.MEDIA_URL, 'elements')
    )

    path = storage.save(file_name, ContentFile(plot_bytes))
    return storage.url(path)


@app.task
def set_element_plot(pk, formula, interval, step):
    points = _get_points(formula, interval, step)

    # creating categories and series from points collection
    categories = []
    series = []
    time_start = datetime.now() - timedelta(days=interval)
    for x, y in points:
        time = time_start + timedelta(hours=x)
        categories.append(time.timestamp())
        series.append(y)

    plot_bytes = _get_plot_bytes(formula, categories, series)
    url = _upload_plot_image(pk, plot_bytes)

    # updating element instance using update without calling save signals
    model = apps.get_model('elements', 'Element')
    model.objects.filter(id=pk).update(plot=url)
