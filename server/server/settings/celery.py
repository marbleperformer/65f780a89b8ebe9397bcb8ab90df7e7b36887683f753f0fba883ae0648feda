import os

BROKER_URL = os.environ.get('BROKER_URL', 'amqp://user:password@rabbitmq:5672/dashboard_vhost')
