from django.urls import path, include
from rest_framework.routers import DefaultRouter

from elements.viewsets import ElementViewSet


router = DefaultRouter()
router.register('elements', ElementViewSet)

urlpatterns = [
    path('', include(router.urls), name='v1')
]
