from django.db import models


class Formula(models.Model):
    expression = models.CharField(max_length=256)

    def __str__(self):
        return self.expression
