from django.urls import path, include
from rest_framework.routers import DefaultRouter

from points.viewsets import PointViewSet


router = DefaultRouter()
router.register('points', PointViewSet, basename='point')

urlpatterns = [
    path('', include(router.urls), name='v1')
]
