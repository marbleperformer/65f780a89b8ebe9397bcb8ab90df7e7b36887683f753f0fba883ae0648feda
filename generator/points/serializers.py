from rest_framework import serializers

from .validators import FormulaValidator
from .models import Formula


class AttrsSerializer(serializers.Serializer):
    formula = serializers.CharField(
        default='t',
        validators=[FormulaValidator(queryset=Formula.objects.all())]
    )
    interval = serializers.IntegerField(default=1)
    step = serializers.IntegerField(default=1)
