from rest_framework import serializers


class FormulaValidator(object):
    def __init__(self, queryset):
        self._queryset = queryset

    def __call__(self, value):
        allowed_formulas = self._queryset.values_list('expression', flat=True)
        if not value in allowed_formulas:
            raise serializers.ValidationError('Wrong formula expression.')