from django.db import models


class Formula(models.Model):
    expression = models.CharField(max_length=256)

    class Meta:
        managed = False
        db_table = 'formulas_formula'

    def __str__(self):
        return self.expression
