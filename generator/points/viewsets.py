from django.db import connection
from rest_framework.viewsets import ViewSet
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status

from .serializers import AttrsSerializer


class PointViewSet(ViewSet):
    @action(detail=False, methods=['post'])
    def series(self, request):
        serializer = AttrsSerializer(data=request.data)
        if serializer.is_valid():
            with connection.cursor() as cursor:
                template = 'SELECT t, {formula} from generate_series(1, {interval}, {step}) as t;'
                query = template.format(**serializer.data)
                cursor.execute(query)
                points = cursor.fetchall()
                return Response({'points': points}, status=status.HTTP_200_OK)
        return Response({'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
